package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"path/filepath"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/robfig/cron/v3"
	"ponybot/consts"
)

var userMap map[int64]string

func main() {
	userMap = make(map[int64]string)
	userMap[423123212] = "yes"
	userMap[464972739] = "yes"

	bot, err := tgbotapi.NewBotAPI("5339041936:AAE-zGJYE27W4YXYInGlZu2sfnwQq5HAEcU")
	if err != nil {
		log.Panic(err)
	}

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)

	var filenames []string
	files, err := ioutil.ReadDir(consts.ImagesDirPath)
	if err != nil {
		fmt.Println(err)
	}
	for _, file := range files {
		filenames = append(filenames, file.Name())
	}

	startDailyCongratulations(bot, filenames)

	for update := range updates {
		if update.Message != nil {
			fmt.Println(userMap)
			rand.Seed(time.Now().UnixNano())
			var message string
			randMes := rand.Intn(50)
			if randMes == 5 {
				message = "Спасибо, что продолжаешь пользоваться ботом! Ты всегда можешь поблагодарить создателя, например обняв его или сказав что-то приятное!"
			} else {
				message = update.Message.From.FirstName + ", " + consts.GoodPhrases[rand.Intn(len(consts.GoodPhrases))]
			}

			//msgText := tgbotapi.NewMessage(update.Message.Chat.ID, message)
			//
			//_, err = bot.Send(msgText)
			//if err != nil {
			//	fmt.Println(err)
			//}

			imgPath := filepath.Join(consts.ImagesDirPath, filenames[rand.Intn(len(filenames))])
			data, _ := ioutil.ReadFile(imgPath)
			b := tgbotapi.FileBytes{Name: "test", Bytes: data}
			im := tgbotapi.NewPhoto(update.Message.Chat.ID, b)
			im.Caption = message
			_, err := bot.Send(im)
			if err != nil {
				fmt.Println(err)
			}
		}
	}
}

//func saveUser(id int64) {
//	if _, ok := userMap[id]; ok {
//		return
//	} else {
//		userMap[id] = "yes"
//	}
//}

func startDailyCongratulations(bot *tgbotapi.BotAPI, filenames []string) {
	cronInstance := cron.New()
	_, err := cronInstance.AddFunc("30 13 * * *", func() {
		sendDailyMessage(bot, filenames)
	})
	if err != nil {
		fmt.Println(err)
		return
	}
	cronInstance.Start()
}

func sendDailyMessage(bot *tgbotapi.BotAPI, filenames []string) {
	for id := range userMap {
		//message := tgbotapi.NewMessage(id, consts.DailyMessages[time.Now().Day()-1])
		//_, err := bot.Send(message)
		//if err != nil {
		//	fmt.Println(err)
		//}
		imgPath := filepath.Join(consts.ImagesDirPath, filenames[rand.Intn(len(filenames))])
		data, _ := ioutil.ReadFile(imgPath)
		b := tgbotapi.FileBytes{Name: "test", Bytes: data}
		im := tgbotapi.NewPhoto(id, b)
		im.Caption = consts.DailyMessages[time.Now().Day()-1]
		_, err := bot.Send(im)
		if err != nil {
			fmt.Println(err)
		}
	}

	return
}
