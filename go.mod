module ponybot

go 1.18

require (
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	github.com/robfig/cron/v3 v3.0.0
)
