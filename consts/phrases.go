package consts

var GoodPhrases = []string{
	"Хорошего тебе дня!",
	"Ваще нормально всё будет!",
	"Пускай тебе повезет!",
	"Счастья, здоровья, здоровья, счастья",
	"Вот это ты конечно нормально выглядишь",
	"Ох блин круто сегодня погодка такая крутая",
	"Опять сидишь бота крутишь...",
	"Иго-го, и удачи!",
	"Жизнь ваще балдеж, кайфуй",
	"Иди покушай вкусненького",
	"Пусть всё прикольно будет!",
	"Ты молодец!",
	"Твое сообщение очень приятное! Тебе тоже всего хорошего!",
	"У тебя всё получится! Нужно только чуть-чуть усилий!",
	"Как дела? Надеюсь, что всё отлично, ведь иначе и быть не может!",
	"Спасибо тебе за то, что ты есть!",
	"Улыбнись, ведь твоя улыбка делает этот мир лучше!",
	"Иногда для счастья нужно так мало... Даже если это просто твоё сообщение!",
	"Пусть твоя жизнь будет такой же прекрасной, как песни Валерия Меладзе!",
	"Никогда не сдавайся! Ты сможешь всё! Ведь это же ты!",
	"Желаю тебе самых вкусных чебуреков!",
	"Надеюсь, что ты не перестанешь мне писать! Спасибо за сообщение, хорошего тебе дня!",
	"Даже если этот день не задался, помни, что за темной полосой обязательно следует светлая!",
	"Пусть в твоём желудке обитает только самая вкусная еда!",
	"Ты пиши мне, пиши! И пусть весь мир подождёт!",
	"Если очень захотеть, можно свернуть горы! А ты, если захочешь, можешь свернуть целый горный хребет!",
	"Что бы тебе ещё такого пожелать? Может самого вкусного мороженого? Да, с орешками!",
	"Если бы было ЕГЭ по тому, как быть самым крутым человеком, ты бы получила 100 баллов!",
	"Каким бы ни был твой день, он обязательно станет лучше!",
	"Не забывай про физические упражнения! Встань, разомнись, или просто обними кого-нибудь!",
	"Пусть твои мечты обязательно сбудутся! Я вот мечтаю о вкусненьком!",
	"Погоди-погоди? Я не могу в это поверить! Мне только что написал мой самый прекрасный собеседник!",
	"Не грусти, хорошо? Жизнь - как зебра, помнишь? Твоя светлая полоса не за горами!",
	"А ты уже кушала вкусное сегодня? Может хотя бы кофе пила? Если нет - быстро иди исправляться!",
	"Ну же, выше нос! Пони плохого не посоветует!",
	"Выглядишь сегодня ты отлично! Надеюсь завтра будет еще лучше!",
	"Время идёт, а жизнь продолжает оставаться прекрасной, несмотря ни на что. Всё будет отлично!",
	"Зарядись отличным настроением! Не отчаивайся и не пасуй перед трудностями!",
	"Помни, что за каждой тучкой прячется солнце",
	"Никогда не говори никогда! На этом свете для тебя нет ничего невозможного!",
	"Подари мне первый танец, забери меня с собой\nЯ повсюду иностранец и повсюду я, вроде-бы, свой\nСловно лодка в океане, затерялся берег мой\nЯ повсюду иностранец. Забери меня, мама, домой",
	"Вела меня от юга и до севера\nДорога по неведомым краям\nМенялся мир, чего в нём только не было\nА три реки впадали в океан\n",
}

var DailyMessages = []string{
	"Сегодня первый день месяца! Желаю тебе провести его с пользой и радостью! Может быть даже получится реализовать какую-то мечту!",
	"Привет! Как твои дела? Говорят, что хорошая еда поднимает настроение, очень советую!",
	"Ой-ой, третий день месяца всего, а ты уже столько переделала! Я тобой горжусь!",
	"Шалость удалась! Знаешь, а сегодня можно и пошалить немного! Дерзай!",
	"Привет-привет! Как твои дела? Узнала что-то новое сегодня? Я вот считаю, что новый день - новое открытие!",
	"Сегодня шестой день месяца! Поздравляю тебя, что ли! Кстати, у кубика тоже шесть сторон, отличный повод чтобы его разобрать.",
	"Какая бы не была погода за окном, и чтобы не скрывалось на душе, пусть этот день принесет только приятные изменения. Пусть он порадует неожиданными интересными встречами, откроет новые перспективы, разрешит старые проблемы.",
	"Самое замечательное настроение пусть сегодня придет к тебе в гости, чтобы твоя улыбка разогнала все беды и не оставила им шанса вернуться.",
	"Спасибо большое за то, что остаешься со мной и читаешь мои сообщения, а не оставляешь их пылиться! Всего самого лучшего тебе сегодня! Тьмок!",
	"Желаю видеть в знамениях судьбы исключительно положительные знаки, извлекать из них пользу и ни при каких обстоятельствах не омрачать своё настроение грустными и тяжкими думами!",
	"Привет! Получай удовольствие от каждой минуты этого дня! Не зацикливайся на проблемах, лучше настраивайся на победы. Я в тебя верю!",
	"Дюжина дней в этом месяце прошла! Поздравляю! Знаю, повод не особо знаменательный, но ведь так здорово устраивать себе праздник каждый день!",
	"Ухх, говорят, что 13 - несчастливое число! Надеюсь, сегодня хотя бы не пятница. Но не давай суевериям затмить твой путь и мешать следовать к своей мечте!",
	"Привет! Желаю тебе провести этот день так, чтобы ни о чем не жалеть! А у тебя получится, я знаю!",
	"Ого, середина месяца уже! Знаешь, время так быстро летит, что и не успеваешь замечать. И немудрено, ведь вокруг столько всего интересного!",
	"Привет! Пусть этот день подарит тебе успех в делах, приятное общение и душевное спокойствие.",
	"Поздравляю тебя с отпуском! Ты заслужила этот отдых! Забудь обо всех проблемах, работе и просто наслаждайся. Удачи тебе!",
	"Желаю, чтобы сегодняшний день превратился во вкусный безглютеновый торт, начинка у которого – блеск твоих счастливых глаз и искренняя радость!",
	"Остановись на секундочку — и оглянись! Сегодня хороший день! Этот день будет еще лучше, если ты сейчас выдохнешь на минутку все свои заботы, вздохнешь полной грудью, вспомнишь какая ты замечательная, вспомнишь самое приятное, что есть у тебя в жизни, и улыбнешься! Хорошего дня!",
	"Пусть этот день принесет тебе миллион сюрпризов и приятных впечатлений. Пусть ярко светит солнышко и поет душа. Плодотворного, насыщенного и легкого!",
	"Пусть этот день наполнит твою жизнь радостью, твои глаза – светом, твою душу – счастьем. Осознай в этот день, что жизнь прекрасна!",
	"Желаю тебе хорошего дня, пусть всё складывается так, как задумано. Удачи во всех делах! И улыбнись!",
	"Чудесный день, такой же, как и ты. Посылаю тебе воздушный поцелуй, и желаю удачного, интересного и красочного дня!",
	"Нечего хандрить и вешать нос. Желаю тебе сегодня отличного настроения и великолепного состояния души, когда хочется петь, танцевать, смеяться, творить, наслаждаться жизнью и делать что-то важное. Всё обязательно получится.",
	"Твой нрав – кофейные зёрна. Твоя улыбка – сахар. Твой взгляд – сливки. Вместе они создают чудесный, ароматный кофе, побуждающий творить шедевры.",
	"Желаю тебе на сегодня, мой дорогой человек, хорошего настроения. И пусть ничего не волнуют твою душу. Будь сегодня на своей волне, верь в себя, вдохновляйся с каждым мгновением и вдохновляй всех вокруг!",
	"Радость моя, спешу скорее пожелать тебе хорошего настроения на сегодня. Помни, ты можешь все, ты - человек невероятной силы и харизмы, великой мечты и удачи.",
	"Пусть этот день будет не просто очередным днем, а запомнится ярким голубым небом, теплым ласковым ветром, чьей-то доброй улыбкой или просто красивой песней, услышанной где-то.",
	"Пусть твои глаза сияют от счастья и искрятся красотой. Вся работа идет как по маслу, и день пройдет просто прекрасно.",
	"Вот и месяц подошел к концу. Возможно, сегодня и не последний его день, а может и так. Самое время оглянуться назад и порадоваться тому, как много ты успела за это время. Я горжусь тобой, ты чудо!",
	"А вот сегодня точно последний день! Получается, что завтра - новое начало! Удачи тебе, ты всё сможешь!",
}
