FROM golang:1.18.0-buster as builder

WORKDIR /ponybot/cmd

COPY . /ponybot

RUN CGO_ENABLED=0 go build -o main .

FROM alpine:3.14

WORKDIR /ponybot/cmd

COPY --from=builder /ponybot/cmd/main /ponybot/cmd/main
COPY --from=builder /ponybot/images /images

ENTRYPOINT ["./main"]